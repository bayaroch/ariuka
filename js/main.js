
$(document).ready(function(){

(function() {
	var triggerBttn = document.getElementById( 'menu-toggle' ),
		overlay = document.querySelector( 'div.overlay' ),
		closeBttn = overlay.querySelector( 'button.overlay-close' );
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = { transitions : Modernizr.csstransitions };

	function toggleOverlay() {
		if( classie.has( overlay, 'open' ) ) {
			$('#menu-toggle').removeClass("open");
			$('.gh-svg-wrapper').removeClass("active");
			classie.remove( overlay, 'open' );
			classie.add( overlay, 'close' );
			var onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName !== 'visibility' ) return;
					this.removeEventListener( transEndEventName, onEndTransitionFn );
				}
				classie.remove( overlay, 'close' );
			};
			if( support.transitions ) {
				overlay.addEventListener( transEndEventName, onEndTransitionFn );
			}
			else {
				onEndTransitionFn();
				$('#menu-toggle').addClass("open");
				$('.gh-svg-wrapper').addClass("active");
			}
		}
		else if( !classie.has( overlay, 'close' ) ) {
			classie.add( overlay, 'open' );
			$('#menu-toggle').addClass("open");
			$('.gh-svg-wrapper').addClass("active");
		}
	}

	triggerBttn.addEventListener( 'click', toggleOverlay );
})();



   function aboutTop() {
    var windowHeight = $(window).outerHeight();
    var headerHeight = $(".page-head").outerHeight();
    var totalHeight = windowHeight - headerHeight;
    $(".about-top").css({"min-height":""+totalHeight+"px"});
    console.log(totalHeight);
   }
   aboutTop();

   $(window).resize(function() {
       aboutTop();
});


    /* Every time the window is scrolled ... */
    $(window).scroll( function(){
    
        /* Check the location of each desired element */
        $('.hideme').each( function(i){
            
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it it */
            if( bottom_of_window > bottom_of_object ){
                
                $(this).animate({'opacity':'1'},500);
                    
            }
            
        }); 
    
    });

 
    $('.fadein').each( function(i){
            /* If the object is completely visible in the window, fade it it */
        
                
                $(this).animate({'opacity':'1'},1000);
                    
            
        }); 
    
    var bodyposition = $("body").offset().top;

       $(window).scroll( function(){
	
	if($(window).scrollTop() > bodyposition ) {
		$(".mouse-down").addClass("hidden");	
	} else {
		$(".mouse-down").removeClass("hidden");
	}
});


   var bodyheight = $(".body").outerHeight();
      $(window).scroll(function(){
		if ($(this).scrollTop() + $(window).height() > $(document).height() - 200) {
			$('.scroll-top').fadeIn();
		} else {
			$('.scroll-top').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scroll-top').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});


});